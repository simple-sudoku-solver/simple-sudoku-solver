#include "values.h"
#include "value.h"
char fill_grid(unsigned short *grid, char* s, unsigned char *remaining)
{
	unsigned char i=0;
    char c;

	while(i < 81) // 9 × 9
	{
		c = s[i] - '0';
		if( c > -1 && c < 10)
		{
			if(c == 0)
			{
				grid[i] = _ALL;
			}
			else
			{
				grid[i] = value(c) | RESULT_FIXED;
				//printf("\rset %d, 689\n", i);
				--*remaining;
			}
		}
		else
            return 0;
		++i;
	}
	i = 0;
	return 1;
}
