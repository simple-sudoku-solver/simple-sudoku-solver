#include "values.h"

char col(const unsigned short n, unsigned short *grid, const char i)
{
	char x = i%9, y=i/9, Yi = 0;

	while (Yi < 9)
	{
		if( Yi == y )
		{
			++Yi;
			if (Yi == 9)
				break;
		}

		if(grid[9*Yi + x] == ( n | RESULT_FIXED ))
			return 1;
//		else if(!(grid[9*Yi + x] |

		++Yi;
	}
	return 0;
}

char row(const unsigned short n, unsigned short *grid, const char i)
{
	char x = i%9, y=i/9, Xi = 0;

	while (Xi < 9)
	{
		if( Xi == x )
		{
			++Xi;
			if(Xi == 9)
				break;
		}

		if(grid[9*y + Xi] == ( n | RESULT_FIXED ))
			return 1;

		++Xi;
	}
	return 0;
}

char region(const unsigned short n, unsigned short *grid, const char i)
{
	char
		x = i%9,
		y = i/9,
		Xi = REGION_ORIG_X(x),
		Yi = REGION_ORIG_Y(y),
		Xmax = Xi + 3,
		Ymax = Yi + 3;

	while (Xi < Xmax)
	{
		while (Yi < Ymax)
		{
			if (Yi == y  && Xi == x)
			{
				++Yi;
				if (Yi == Ymax)
					break;
			}

			if(grid[Yi * 9 + Xi] == ( n | RESULT_FIXED ))
				return 1;
			++Yi;
		}
		Yi-=3;
		++Xi;
	}
	return 0;
}
