#include "values.h"
#include "core.h"

char number_of_possibilities(unsigned short n)
{
	char l=0;
	if(n & _1)
		l++;
	if(n & _2)
		l++;
	if(n & _3)
		l++;
	if(n & _4)
		l++;
	if(n & _5)
		l++;
	if(n & _6)
		l++;
	if(n & _7)
		l++;
	if(n & _8)
		l++;
	if(n & _9)
		l++;
	return l;
}

char is_in_grid_part(unsigned short n, unsigned short *grid_part, char grid_part_len, unsigned char *coords, unsigned char *grid_part_coords)
{
	char i=0,nb=0;
	while(i < grid_part_len)
	{
		if(grid_part[i] == n)
		{
			coords[nb] = grid_part_coords[i];
			++nb;
		}
		++i;
	}

	return nb;
}

char coord_is_not_in_array(unsigned char coord, unsigned char *coords, char coords_len)
{
	char i=0;
	while(i < coords_len)
	{
		if(coords[i] == coord)
			return 0;
		++i;
	}
	return 1;
}

void strip_possibilities(const unsigned short n, unsigned short *cell, unsigned char *remaining)
{
	if(n & _1)
		strip_possibility(_1, cell, remaining);
	if(n & _2)
		strip_possibility(_2, cell, remaining);
	if(n & _3)
		strip_possibility(_3, cell, remaining);
	if(n & _4)
		strip_possibility(_4, cell, remaining);
	if(n & _5)
		strip_possibility(_5, cell, remaining);
	if(n & _6)
		strip_possibility(_6, cell, remaining);
	if(n & _7)
		strip_possibility(_7, cell, remaining);
	if(n & _8)
		strip_possibility(_8, cell, remaining);
	if(n & _9)
		strip_possibility(_9, cell, remaining);
}
