void block_interactions(unsigned short *grid, unsigned char *remaining);
char line_of_region(const unsigned short n, unsigned short *grid, const char i);
char col_of_region(const unsigned short n, unsigned short *grid, const char i);
void naked_subset(unsigned short *grid, unsigned char *remaining);
void disjoin_chain(unsigned short *grid, char x, char y, unsigned char *remaining);
