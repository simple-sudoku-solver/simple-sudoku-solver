#include "check.h"
#include "values.h"
#include "core.h"
#include "stdio.h"
void clone_grid(unsigned short *from, unsigned short *to)
{
	unsigned char i = 0;
	while(i < 81)
	{
		to[i] = from[i];
		++i;
	}
}

unsigned take_value(unsigned short *cell, unsigned val)
{ // val is the last returned value, or 0
	unsigned newval;
	if(val == 0)
		newval = 1;
	else
		newval = val * 2;

	while(!(*cell & newval) && newval <= _9)
	{
		newval*=2;
	}
	if(newval & *cell)
		return newval;
	else
		return 0;
}

char sudoku_brute_fill_grid(unsigned short *grid, unsigned short *grid_force, unsigned char i, unsigned char *remaining, unsigned char o)
{
	unsigned val = 0;
	unsigned char r = *remaining;

	if(grid_force[i] == 0)
		return 0;

	find_posibilities(grid_force, i, remaining, o);
	while(grid_force[i] & RESULT_FIXED)
	{
		++i;
		find_posibilities(grid_force, i, remaining, o);
		if(grid_force[i] == 0)
			return 0;
	}

	if(i < 81)
	{
		--*remaining;
		while((val = take_value(&grid_force[i], val)))
		{
			if(sudoku_brute_fill_grid(grid, grid_force, i+1, remaining, o))
				return 1;
		}
		++*remaining;
	}
	else
	{
		resolve_sudoku(grid_force, remaining, o);
		printf("%d\n",*remaining);
		if(*remaining)
		{
			grid_force[i] = grid[i],
			*remaining = r;
			return 0;
		}
		else
			return 1;
	}

	grid_force[i] = grid[i],
	*remaining = r;
	return 0;
}

char sudoku_brute_force(unsigned short *grid, unsigned char *remaining, unsigned char o)
{
	unsigned short grid_force[81];
	printf("R : %d\n", *remaining);
	clone_grid(grid, grid_force);
	if(sudoku_brute_fill_grid(grid, grid_force, 0, remaining, o))
	{
		clone_grid(grid_force, grid);
		return 1;
	}
	return 0;
}
