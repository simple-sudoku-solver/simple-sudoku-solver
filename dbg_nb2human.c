#include <stdio.h>
#include <stdlib.h>

#define _1 1
#define _2 2
#define _3 4
#define _4 8
#define _5 16
#define _6 32
#define _7 64
#define _8 128
#define _9 256
#define RESULT_FIXED 512

char DBG_possibilities(long n)
{
	if(n & _1)
		printf("1  ");
	if(n & _2)
		printf("2  ");
	if(n & _3)
		printf("3  ");
	if(n & _4)
		printf("4  ");
	if(n & _5)
		printf("5  ");
	if(n & _6)
		printf("6  ");
	if(n & _7)
		printf("7  ");
	if(n & _8)
		printf("8  ");
	if(n & _9)
		printf("9  ");
	if(n & RESULT_FIXED)
		printf(	"result fixed.");
	return 0;
}

int main(int argc, char **argv)
{
	if(argc > 1)
	{
		int i=1;
		while(i < argc)
		{
			printf("%d	", strtol(argv[i], NULL, 10));
			DBG_possibilities(strtol(argv[i], NULL, 10));
			printf("\n");
			++i;
		}
	}
	return 0;
}
