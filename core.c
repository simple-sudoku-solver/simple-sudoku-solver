#include "values.h"
#include "value.h"
#include "config.h"
#include "basic.h"

#ifndef EASY_ENGINE
	#include "difficult.h"
#endif

#ifndef DISABLE_DIABOLIC
	#include "diabolic.h"
#endif

char strip_possibility(const unsigned short n, unsigned short *cell, unsigned char *remaining) // return val stripped of its n possibility
{
	if(*cell & n)
	{
#ifdef DBG
		if(*cell & RESULT_FIXED)
		{
			printf("\nTIRED TO STRIP FINAL_RESULT !\n");
			exit(0);
		}
#endif

		*cell ^= n;

// Find out if there is only one possibility remaining
		switch(*cell)
		{
		case _1:
		case _2:
		case _3:
		case _4:
		case _5:
		case _6:
		case _7:
		case _8:
		case _9:
			*cell |= RESULT_FIXED;
			--*remaining;
			return 1;
		}
		return 0;
	}
	return 0;
}

char forbidden_number(const unsigned short n, unsigned short *grid, const char i)
{
	return
	    row(n, grid, i)
	 || col(n, grid, i)
	 || region(n, grid, i)
#ifndef DISABLE_DIABOLIC
	 || line_of_region(n, grid, i)
	 || col_of_region(n, grid, i)
#endif
	;
}

void find_posibilities (unsigned short *grid, const char i, unsigned char *remaining)
{
	char j=1;
	unsigned short n = _1;
	while ( j < 10)
	{
		if(grid[i] & n)
		{
			if(forbidden_number(n, grid, i))
			{
				if(strip_possibility(n, &grid[i], remaining))
				{
					//printf("\rset %d, 542\n", i);
					return;
				}
			}

#ifndef EASY_ENGINE

			else if (only_possibility_in_square(n, grid, i))
			{
				--*remaining;
				grid[i] = n | RESULT_FIXED;
						//printf("\rset %d, 551\n", i);
				return;
			}

#endif
		}
		++j;
		n = value(j);
	}
}

unsigned short resolve_sudoku(unsigned short *grid, unsigned char *remaining, unsigned char o)
{
	unsigned char i;
	unsigned short iteration = 0;

	while(*remaining)
	{
		i=0;

		#ifndef DISABLE_DIABOLIC
			block_interactions(grid, remaining);
			naked_subset(grid, remaining);
		#endif

		while(*remaining && i < 81)
		{
			if(!(grid[i] & RESULT_FIXED))
			{
#ifndef EASY_ENGINE
				strip_possibilities_around(grid, i, remaining, o);
#endif
				find_posibilities(grid, i, remaining);
			}

			++i;
		}

		if(iteration == MAX_ITERATION)
			break;

		++iteration;
	}
	return iteration;
}
