#include "values.h"
#include "value.h"
#include "core.h"
/* These functions are useless for easy sudokus,
 * they may improve performances for medium sudokus (I'm not sure al ALL),
 * they are required by diffucult sudokus */

/* Find out if a given number is only possible in this line and in this
 * region, and strip possibilibies to the others cells of the region
 */

void only_in_this_col (unsigned short *grid, const char i, unsigned char *remaining, const unsigned short n)
{
	char
		x = i%9,
		y = 0,
		Yi = REGION_ORIG_Y(i/9),
		Ymax = Yi + 3;
//	printf("\rn = %d ; Yi = %d, Ymax = %d, x = %d, y = %d\n", n, Yi, Ymax, x, i/9);
	while(y < 9)
	{
//		printf("(%d, %d) ... ", x, y);
		if(y >= Ymax || y < Yi) // checking that we are not in the square
		{
			if(grid[y * 9 + x] & n) // if the value is found out of the square
			{
//				printf("return.\n");
				return;
			}
//			printf("continue.\n");
		}
		else
		{
//			printf("skip.\n");
		}
		++y;
	}
//	printf("2.\n");
	char
		Xi = REGION_ORIG_X(x),
		Xmax = Xi + 3;

	while(Xi < Xmax)
	{
		while (Yi < Ymax)
		{
//			printf("(%d, %d) ... ", Xi, Yi);
			if(Xi != x)
			{
//				printf("strip. cell & n : %d \n", grid[Yi*9 + Xi] & n);
				strip_possibility(n, &grid[Yi*9 + Xi], remaining);
//				printf("OK col\n");
			}

			++Yi;
		}
		Yi -=3;
		++Xi;
	}
}

void only_in_this_line (unsigned short *grid, const char i, unsigned char *remaining, const unsigned short n)
{
	char
		x = 0,
		y = i/9,
		Xi = REGION_ORIG_X(i%9),
		Xmax = Xi + 3;

	while(x < 9)
	{
		if(x >= Xmax || x < Xi)
		{
			if(grid[y * 9 + x] & n)
				return;
		}
		++x;
	}

	char
		Yi = REGION_ORIG_Y(y),
		Ymax = Yi + 3;

	while(Xi < Xmax)
	{
		while (Yi < Ymax)
		{
			if(Yi != y)
			{
//				printf("\rstrip line :\r");
				strip_possibility(n, &grid[Yi*9 + Xi], remaining);
//				printf("OK line\n");
			}
			++Yi;
		}
		Yi -=3;
		++Xi;
	}
}

/* find out if the given cell is the only solution of the region for a given nomber */
char only_possibility_in_square(const unsigned short n, unsigned short *grid, const char i)
{
	char
		x = i%9,
		y = i/9,
		Xi = REGION_ORIG_X(x),
		Yi = REGION_ORIG_Y(y),
		Xmax = Xi + 3,
		Ymax = Yi + 3;

	while (Xi < Xmax)
	{
		while (Yi < Ymax)
		{
			if (Yi == y  && Xi == x)
			{
				++Yi;
				if (Yi == Ymax)
					break;
			}

			if(grid[Yi * 9 + Xi] & n)
			{
				return 0;
			}
			++Yi;
		}
		Yi-=3;
		++Xi;
	}
	return 1;
}

// Will try to strip possibilities for other cells of the region
void strip_possibilities_around (unsigned short *grid, const char i, unsigned char *remaining)
{
	char j = 1;
	unsigned short n = _1;

	while( j < 9 )
	{
		if (grid[i] & n)
		{
			only_in_this_col(grid, i, remaining, n);
			only_in_this_line(grid, i, remaining, n);
		}
		++j;
		n = value(j);
	}
}
