// Examples of grid :
// 100007090030020008009600500005300900010080002600004000300000010040000007007000300 "AI Escargot"

// 000050000800900000700120300003002004680010000042000080000004002007000043005070600 diabolic
// 070400030000000059500601000080950000000030000000200800006000300809020047000700006 difficult
// 580003000920004100000008350709020008002000700300050201046700000005900072000300046 medium
// 006700409507091060094080070250009600900057380008316002602000500700960000040008030 easy

//FIXME : implémenter sudoku X, genre :
// 802000091603900000000002064008369020000805000020714900280600000000003409390000206
// Options : comment out options you don't want. You might not want to activate one of them.

	/* strip ability to resolve difficult and diabolic sudokus
	 * In order to optimize resolving easy sudokus
	 * Resolves medium sudokus too.
 	 */
//#define EASY_ENGINE

	/* strip ability to resolve diabolic sudokus
	 * In order to optimize resolving others sudokus
 	 */
//#define DISABLE_DIABOLIC

	/* Turn on Debugging mode. NOT RECOMMENDED AT ALL. */
#define DBG
//#define DBG_GRID // show values of not-found cell
//#define DBG_POSSIBILITIES // show possibilities while checking for all cells

// values defining program's behavior
#define MAX_ITERATION 1000

// Internal constants :
#define _1 1
#define _2 2
#define _3 4
#define _4 8
#define _5 16
#define _6 32
#define _7 64
#define _8 128
#define _9 256
#define RESULT_FIXED 512

#define QUIET 1
#define CHECK 2

#define REGION_ORIG_X(x) x/3 * 3
#define REGION_ORIG_Y(y) y/3 * 3

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARG(a) (strcmp(argv[j], "-" #a) == 0 || strcmp(argv[j], "--" #a) == 0)

char human(unsigned short n)
{
	if(n & _1)
		return 1;
	if(n & _2)
		return 2;
	if(n & _3)
		return 3;
	if(n & _4)
		return 4;
	if(n & _5)
		return 5;
	if(n & _6)
		return 6;
	if(n & _7)
		return 7;
	if(n & _8)
		return 8;
	if(n & _9)
		return 9;
	return 0;
}

char DBG_possibilities(long n)
{
	if(n & _1)
		printf("1  ");
	if(n & _2)
		printf("2  ");
	if(n & _3)
		printf("3  ");
	if(n & _4)
		printf("4  ");
	if(n & _5)
		printf("5  ");
	if(n & _6)
		printf("6  ");
	if(n & _7)
		printf("7  ");
	if(n & _8)
		printf("8  ");
	if(n & _9)
		printf("9  ");
	if(n & RESULT_FIXED)
		printf(	"result fixed.");
	return 0;
}

void print_grid(unsigned short *grid)
{
	unsigned char i=0;
	while(i < 81)
	{
		if(i%3 == 0)
		{
			if(i%9 == 0)
			{
				if(i)
				{
					printf("|\n");
				}
				if(i%27 == 0)
				{
					printf("-------------------------------\n");
				}
			}
			printf("|");
		}
		if(grid[i] & RESULT_FIXED)
		{
			printf(" %d ", human(grid[i]));
		}
		else
#ifdef DBG_GRID
			printf("?%d",grid[i]);
#else
			printf(" ° ");
#endif

		++i;
	}
	printf("|\n");
	printf("-------------------------------\n");	
}

short value(char n)
{
	if(n == 1)
		return _1;
	if(n == 2)
		return _2;
	if(n == 3)
		return _3;
	if(n == 4)
		return _4;
	if(n == 5)
		return _5;
	if(n == 6)
		return _6;
	if(n == 7)
		return _7;
	if(n == 8)
		return _8;
	if(n == 9)
		return _9;
	return 0;
}

void fill_possibilities(unsigned short *cell)
{
		*cell = _1 | _2 | _3 | _4 | _5 | _6 | _7 | _8 | _9;
}

char strip_possibility(const unsigned short n, unsigned short *cell, unsigned char *remaining) // return val stripped of its n possibility
{
	if(*cell & n)
	{
		if(*cell & RESULT_FIXED)
		{
			printf("\nTIRED TO STRIP FINAL_RESULT !\n");
			exit(0);
		}
			
		*cell ^= n;

// Find out if there is only one possibility remaining
		switch(*cell)
		{
		case _1:
		case _2:
		case _3:
		case _4:
		case _5:
		case _6:
		case _7:
		case _8:
		case _9:
			*cell |= RESULT_FIXED;
			--*remaining;
			return 1;
		}
		return 0;
	}
	return 0;
}

char region(const unsigned short n, unsigned short *grid, const char i)
{
	char
		x = i%9,
		y = i/9,
		Xi = REGION_ORIG_X(x),
		Yi = REGION_ORIG_Y(y),
		Xmax = Xi + 3,
		Ymax = Yi + 3;

	while (Xi < Xmax)
	{
		while (Yi < Ymax)
		{
			if (Yi == y  && Xi == x)
			{
				++Yi;
				if (Yi == Ymax)
					break;
			}

			if(grid[Yi * 9 + Xi] == ( n | RESULT_FIXED ))
				return 1;
			++Yi;
		}
		Yi-=3;
		++Xi;
	}
	return 0;
}

//FIXME: these 2 functions seem useless
char line_of_region(const unsigned short n, unsigned short *grid, const char i)
{
	char
		x = i%9,
		y = i/9,
		Xi = 0,
		Yi = REGION_ORIG_Y(y),
		Xmax = 3,
		Ymax = Yi + 3;

	while (Xmax < 9)
	{
		while (Xi < Xmax)
		{
			while (Yi < Ymax)
			{
				//FIXME : useless ?
				if (Yi == y  && Xi == x)
				{
					++Yi;
					if (Yi == Ymax)
						break;
				}

				if(Xi == x)
				{
					if(!(grid[Yi * 9 + Xi] & n))
						return 0;
				}
				else if (grid[Yi * 9 + Xi] & n)
					return 0;

				++Yi;
			}
			Yi-=3;
			++Xi;
		}
		Xmax+=3;
	}
	printf("\rFORBIDDEN 2\n");

	return 1;
}

char col_of_region(const unsigned short n, unsigned short *grid, const char i)
{
	char
		x = i%9,
		y = i/9,
		Xi = REGION_ORIG_X(x),
		Yi = 0,
		Xmax = Xi + 3,
		Ymax = 2;

	while (Ymax < 9)
	{
		while (Yi < Ymax)
		{
			while (Xi < Xmax)
			{
				//FIXME : useless ?
				if (Yi == y  && Xi == x)
				{
					++Xi;
					if (Xi == Xmax)
						break;
				}

				if(Yi == y)
				{
					if(!(grid[Yi * 9 + Xi] & n))
						return 0;
				}
				else if (grid[Yi * 9 + Xi] & n )
					return 0;

				++Xi;
			}
			Yi-=3;
			++Yi;
		}
		Ymax+=3;
	}
	printf("\rFORBIDDEN 1\n");
	return 1;
}

char col(const unsigned short n, unsigned short *grid, const char i)
{
	char x = i%9, y=i/9, Yi = 0;

	while (Yi < 9)
	{
		if( Yi == y )
		{
			++Yi;
			if (Yi == 9)
				break;
		}

		if(grid[9*Yi + x] == ( n | RESULT_FIXED ))
			return 1;
//		else if(!(grid[9*Yi + x] | 

		++Yi;
	}
	return 0;
}

char row(const unsigned short n, unsigned short *grid, const char i)
{
	char x = i%9, y=i/9, Xi = 0;

	while (Xi < 9)
	{
		if( Xi == x )
		{
			++Xi;
			if(Xi == 9)
				break;
		}

		if(grid[9*y + Xi] == ( n | RESULT_FIXED ))
			return 1;

		++Xi;
	}
	return 0;
}

#ifndef EASY_ENGINE

/* These functions are useless for easy sudokus,
 * they may improve performances for medium sudokus (I'm not sure al ALL),
 * they are required by diffucult sudokus */

/* Find out if a given number is only possible in this line and in this
 * region, and strip possibilibies to the others cells of the region
 */

void only_in_this_col (unsigned short *grid, const char i, unsigned char *remaining, const unsigned char o, const unsigned short n)
{
	char
		x = i%9,
		y = 0,
		Yi = REGION_ORIG_Y(i/9),
		Ymax = Yi + 3;
//	printf("\rn = %d ; Yi = %d, Ymax = %d, x = %d, y = %d\n", n, Yi, Ymax, x, i/9);
	while(y < 9)
	{
//		printf("(%d, %d) ... ", x, y);
		if(y >= Ymax || y < Yi) // checking that we are not in the square
		{
			if(grid[y * 9 + x] & n) // if the value is found out of the square
			{
//				printf("return.\n");
				return;
			}
//			printf("continue.\n");
		}
		else
		{
//			printf("skip.\n");
		}
		++y;
	}
//	printf("2.\n");
	char
		Xi = REGION_ORIG_X(x),
		Xmax = Xi + 3;

	while(Xi < Xmax)
	{
		while (Yi < Ymax)
		{
//			printf("(%d, %d) ... ", Xi, Yi);
			if(Xi != x)
			{
//				printf("strip. cell & n : %d \n", grid[Yi*9 + Xi] & n);
				strip_possibility(n, &grid[Yi*9 + Xi], remaining);
//				printf("OK col\n");
			}
			else
			{
//				printf("skip.\n");
			}
			++Yi;
		}
		Yi -=3;
		++Xi;
	}
}

void only_in_this_line (unsigned short *grid, const char i, unsigned char *remaining, const unsigned char o, const unsigned short n)
{
	char
		x = 0,
		y = i/9,
		Xi = REGION_ORIG_X(i%9),
		Xmax = Xi + 3;

	while(x < 9)
	{
		if(x >= Xmax || x < Xi)
		{
			if(grid[y * 9 + x] & n)
				return;
		}
		++x;
	}

	char
		Yi = REGION_ORIG_Y(y),
		Ymax = Yi + 3;

	while(Xi < Xmax)
	{
		while (Yi < Ymax)
		{
			if(Yi != y)
			{
//				printf("\rstrip line :\r");
				strip_possibility(n, &grid[Yi*9 + Xi], remaining);
//				printf("OK line\n");
			}
			++Yi;
		}
		Yi -=3;
		++Xi;
	}
}

// Will try to strip possibilities for other cells of the region
void strip_possibilities_around (unsigned short *grid, const char i, unsigned char *remaining, const unsigned char o)
{
	char j = 1;
	unsigned short n = _1;

	while( j < 9 )
	{
		if (grid[i] & n)
		{
			only_in_this_col(grid, i, remaining, o, n);
			only_in_this_line(grid, i, remaining, o, n);
		}
		++j;
		n = value(j);
	}
}


/* find out if the given cell is the only solution of the region for a given nomber */

char only_possibility_in_square(const unsigned short n, unsigned short *grid, const char i)
{
	char
		x = i%9,
		y = i/9,
		Xi = REGION_ORIG_X(x),
		Yi = REGION_ORIG_Y(y),
		Xmax = Xi + 3,
		Ymax = Yi + 3;

	while (Xi < Xmax)
	{
		while (Yi < Ymax)
		{
			if (Yi == y  && Xi == x)
			{
				++Yi;
				if (Yi == Ymax)
					break;
			}

			if(grid[Yi * 9 + Xi] & n)
			{
				return 0;
			}
			++Yi;
		}
		Yi-=3;
		++Xi;
	}
	return 1;
}
#endif

char forbidden_number(const unsigned short n, unsigned short *grid, const char i)
{
	return 
	    row(n, grid, i)
	 || col(n, grid, i)
	 || region(n, grid, i)
#ifndef EASY_ENGINE
//	 || line_of_region(n, grid, i)
//	 || col_of_region(n, grid, i)
#endif
	;
}

/**
#ifdef DBG
	void dbg_set(const unsigned short *grid, const char i)
	{
//		//printf("\rset (%d,%d) = %d\n", i%9 + 1, i/9 + 1, human(grid[i])); 
	}
	char dbg_trace_set(const char val, const unsigned short *grid,const char i)
	{
		if(val)
		{
//			printf("1: \n");
			dbg_set(grid, i);
		}
		return val;
	}
	#define DBG_TRACE_SET(val) dbg_trace_set(val, grid, i)
#else
	#define DBG_TRACE_SET(i) i
#endif
//*/

void find_posibilities (unsigned short *grid, const char i, unsigned char *remaining, const unsigned char o)
{
	char j=1;
	unsigned short n = _1;
	while ( j < 10)
	{
		if(grid[i] & n)
		{
			if(forbidden_number(n, grid, i))
			{
				if(strip_possibility(n, &grid[i], remaining))
				{
					//printf("\rset %d, 542\n", i);
					return;
				}
			}

#ifndef EASY_ENGINE

			else if (only_possibility_in_square(n, grid, i))
			{
				--*remaining;
				grid[i] = n | RESULT_FIXED;
						//printf("\rset %d, 551\n", i);
				return;
			}

#endif
		}
		++j;
		n = value(j);
	}
}

char grid_check(unsigned short *grid) // return 1 on error
{
	char x=0, y=0;
	short checking = 0, val;

// 1. lines
	while(y < 9)
	{
		x=0;
		checking = 0;
		while (x < 9)
		{
			if(grid[y*9 + x] & RESULT_FIXED)
			{
				val = grid[y*9 + x] ^ RESULT_FIXED;

				if(val & checking)
				{
#ifdef DBG
					printf("\nDBG : grid_checking lines: (%d, %d)\n", x, y);
#endif
					return 1;
				}

				checking |=val;
			}
			++x;
		}
		++y;
	}
	x=0;
// 2. cols
	while(x < 9)
	{
		y=0;
		checking = 0;
		while (y < 9)
		{
			if(grid[y*9 + x] & RESULT_FIXED)
			{
				val = grid[y*9 + x] ^ RESULT_FIXED;

				if(val & checking)
				{
#ifdef DBG
					printf("\nDBG : grid_checking cols: (%d, %d)\n", x, y);
#endif
					return 1;
				}

				checking |=val;
			}

#ifdef DBG_POSSIBILITIES
				printf("(%d,%d)	", x, y);
				DBG_possibilities(grid[y*9 + x]);
				printf("\n");
#endif

			++y;
		}
		++x;
	}

// 3. regions

	char Xstart=0, Ystart;

	while(Xstart < 9)
	{
		Ystart=0;
		while(Ystart < 9)
		{
				checking = 0;
				x=Xstart;

				while (x < Xstart + 3)
				{
						y=Ystart;
						while (y < Ystart + 3)
						{
							if(grid[y*9 + x] & RESULT_FIXED)
							{
								val = grid[y*9 + x] ^ RESULT_FIXED;
								if(val & checking)
								{
#ifdef DBG
									printf("\nDBG : grid_checking region: (%d, %d)\n", x, y);
#endif
									return 1;
								}
								checking |=val;
							}
							++y;
						}
						++x;
				}
				Ystart+=3;
		}
		Xstart+=3;
	}

	return 0;
}

int main (int argc, char ** argv)
{
	unsigned char i = 0,
	     o = 0, // options
	     grid_done = 0,
	     remaining = 81; // remaining values to find

	int j = 1; // loops

	unsigned short grid [81];
long dbgloop = 0;
	// arguments
	while(j < argc)
	{
		if(ARG(q) || ARG(quiet))
		{
			o |= QUIET; // We are quiet
		}
		else if (ARG(h) || ARG(help))
		{
help:			printf(
				"Sudoku solver. Raphaël JAKSE, 2010\n"
				"	sudoku-solver [options] <grid>\n"
				"	<grid> is a string composed of 0 to 9 numbers. \n"
				"	it represent a sudoku game from left to right and from top to bottom.\n"
				"	0 represents an empty value.\n\n"
				"	Available options:\n"
				"		-q,	--quiet: Behave quietly,\n"
				"		             results are given as a string like <grid>\n"
				"		-c,	--check: Check the grid when finish.\n"
				"		             if -q is passed, return EXIT_FAILURE.\n"
				"	You can't combine options, like in -qc. Write -q -c instead.\n"
#ifdef EASY_ENGINE
				"\nThis version was not compiled with support for difficult sudokus.\n"
#endif
			);
			return EXIT_SUCCESS;
		}
		else if (ARG(c) || ARG(check))
		{
			o |= CHECK; // we check results at the end.
		}
		else
		{
			// main argument, grid. checking, parsing argument and filling grid.
			char c; // temp number
			while(i < 81) // 9 × 9 
			{
				c = argv[j][i] - '0';
				if( c > -1 && c < 10)
				{
					if(c == 0)
					{
						fill_possibilities(&grid[i]);
					}
					else
					{
						grid[i] = value(c) | RESULT_FIXED;
						//printf("\rset %d, 689\n", i);
						--remaining;
					}
				}
				else
				{
					c+='0';
					if(c == '\0')
					{
						//fprintf(stderr, "The grid is not complete. You must pass 81 figures.\n.");
						return EXIT_FAILURE;
					}
					else
					{
						//fprintf(stderr, "invalid argument: %c is not a valid value in the Sudoku grid.\n", c);
						return EXIT_FAILURE;
					}
				}
				++i;
			}
			i = 0;
			grid_done = 1; // We have done the grid, we can continue.
		}
		++j;
	}

	if(!grid_done) // Checking whether the grid was done.
	{ // if not, show the help.
		goto help;
	}

	// show grid
	if(!(o & QUIET))
	{
		printf("Working on this grid:\n");

		print_grid(grid);
	}


	// begin thinking
	while(remaining)
	{
		i=0;
		while(remaining && i < 81)
		{
			if(!(grid[i] & RESULT_FIXED))
			{
#ifndef EASY_ENGINE
				strip_possibilities_around(grid, i, &remaining, o);
#endif
				find_posibilities(grid, i, &remaining, o);
			}
			++i;
		}
		printf("Remaining : %d, loop = %ld\r", remaining, dbgloop);

		++dbgloop;
		if(dbgloop == MAX_ITERATION) break;
	}

	// show final grid
	if(!(o & QUIET))
	{//FIXME : implémenter QUIET
		printf("\nResult: \n");
		print_grid(grid);
	}

	if(o & CHECK)
	{
		if(grid_check(grid))
		{
			if(!(o & QUIET))
			{
				printf("\ngrid is not correct !\n\n");
			}
			return EXIT_FAILURE;
		}
		else
		{
			if(!(o & QUIET))
			{
				printf("\ngrid seems correct.\n\n");
			}
		}
	}

	return EXIT_SUCCESS;
}
