#ifndef DIFFICULT_H
	#define DIFFICULT_H

	char only_possibility_in_square(const unsigned short n, unsigned short *grid, const char i);
	void strip_possibilities_around (unsigned short *grid, const char i, unsigned char *remaining, const unsigned char o);
	void only_in_this_line (unsigned short *grid, const char i, unsigned char *remaining, const unsigned char o, const unsigned short n);
	void only_in_this_col (unsigned short *grid, const char i, unsigned char *remaining, const unsigned char o, const unsigned short n);

#endif
