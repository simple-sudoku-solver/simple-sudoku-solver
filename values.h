#ifndef _1
	// Internal constants :
	#define _1 1
	#define _2 2
	#define _3 4
	#define _4 8
	#define _5 16
	#define _6 32
	#define _7 64
	#define _8 128
	#define _9 256

	#define _ALL 511 // = _1 | _2 | _3 | _4 | _5 | _6 | _7 | _8 | _9;

	#define RESULT_FIXED 512

	#define REGION_ORIG_X(x) x/3 * 3
	#define REGION_ORIG_Y(y) y/3 * 3
	#define COORDS(i) i%9, i/9
#endif
