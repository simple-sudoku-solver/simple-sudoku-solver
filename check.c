#include "values.h"
char grid_check(unsigned short *grid) // return 1 on error
{
	char x=0, y=0;
	short checking = 0, val;

// 1. lines
	while(y < 9)
	{
		x=0;
		checking = 0;
		while (x < 9)
		{
			if(grid[y*9 + x] & RESULT_FIXED)
			{
				val = grid[y*9 + x] ^ RESULT_FIXED;

				if(val & checking)
				{
#ifdef DBG
					printf("\nDBG : grid_checking lines: (%d, %d)\n", x, y);
#endif
					return 1;
				}

				checking |=val;
			}
			++x;
		}
		++y;
	}
	x=0;
// 2. cols
	while(x < 9)
	{
		y=0;
		checking = 0;
		while (y < 9)
		{
			if(grid[y*9 + x] & RESULT_FIXED)
			{
				val = grid[y*9 + x] ^ RESULT_FIXED;

				if(val & checking)
				{
#ifdef DBG
					printf("\nDBG : grid_checking cols: (%d, %d)\n", x, y);
#endif
					return 1;
				}

				checking |=val;
			}

#ifdef DBG_POSSIBILITIES
				printf("(%d,%d)	", x, y);
				DBG_possibilities(grid[y*9 + x]);
				printf("\n");
#endif

			++y;
		}
		++x;
	}

// 3. regions

	char Xstart=0, Ystart;

	while(Xstart < 9)
	{
		Ystart=0;
		while(Ystart < 9)
		{
				checking = 0;
				x=Xstart;

				while (x < Xstart + 3)
				{
						y=Ystart;
						while (y < Ystart + 3)
						{
							if(grid[y*9 + x] & RESULT_FIXED)
							{
								val = grid[y*9 + x] ^ RESULT_FIXED;
								if(val & checking)
								{
#ifdef DBG
									printf("\nDBG : grid_checking region: (%d, %d)\n", x, y);
#endif
									return 1;
								}
								checking |=val;
							}
							++y;
						}
						++x;
				}
				Ystart+=3;
		}
		Xstart+=3;
	}

	return 0;
}
