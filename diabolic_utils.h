#ifndef DIABOLIC_UTILS_H
	#define DIABOLIC_UTILS_H
	char number_of_possibilities(unsigned short n);
	char is_in_grid_part(unsigned short n, unsigned short *grid_part, char grid_part_len, unsigned char *coords, unsigned char *grid_part_coords);
	char coord_is_not_in_array(unsigned char coord, unsigned char *coords, char coords_len);
	void strip_possibilities(const unsigned short n, unsigned short *cell, unsigned char *remaining);

	#define common_possibilities(n1,n2) number_of_possibilities(n1 & n2)

#endif
