#ifndef CORE_H
	#define CORE_H

	char strip_possibility(const unsigned short n, unsigned short *cell, unsigned char *remaining);
	char forbidden_number(const unsigned short n, unsigned short *grid, const char i); // for find_possibility only
	void find_posibilities (unsigned short *grid, const char i, unsigned char *remaining, const unsigned char o);
	unsigned short resolve_sudoku(unsigned short *grid, unsigned char *remaining, const unsigned char o);

#endif
