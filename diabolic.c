#include "values.h"
#include "value.h"
#include "core.h"
#include "diabolic_utils.h"

void block_interactions(unsigned short *grid, unsigned char *remaining)
{
    char
        x,
        y,
        XYi,
        Xmax,
        Ymax,
        j=1,
        val,
        found_in_region=0;

	while(j<10)
	{
//		//printf("\rj=%d\n",j);
		val = value(j);

// lines
		y=0;
		while(y<9)
		{
			x=0;
			while(x<9)
			{
				if(grid[y*9 + x] & val)
				{
					if(found_in_region)
					{
						found_in_region=0; // don't act
						break;
					}
					else
						found_in_region = 1 + x / 3;
				}
				++x;
			}

			if(found_in_region) // act
			{
				x=(found_in_region - 1) * 3;
				Xmax = x+3;
				XYi = y / 3 * 3;
				Ymax = XYi+3;
				while(XYi < Ymax)
				{
					if(XYi != y)
					{
						while(x < Xmax)
						{
							if(grid[XYi * 9 + x] & val)
							{
								//printf("\rstripped %d for (%d, %d)\n", j, x, XYi);
								strip_possibility(val,&grid[XYi * 9 + x], remaining);
							}
							++x;
						}
					}
					++XYi;
				}

				found_in_region=0;
			}
			++y;
		}
//cols
		x=0;
		while(x<9)
		{
			y=0;
			while(y<9)
			{
				if(grid[y*9 + x] & val)
				{
					if(found_in_region)
					{
						found_in_region=0; // don't act
						break;
					}
					else
						found_in_region = 1 + y / 3;
				}
				++y;
			}

			if(found_in_region) // act
			{
				y=(found_in_region - 1) * 3;
				Ymax = y+3;
				XYi = x / 3 * 3;
				Xmax = XYi+3;
				while(XYi < Xmax)
				{
					if(XYi != x)
					{
						while(y < Ymax)
						{
							if(grid[y * 9 + XYi] & val)
							{
								//printf("\rstripped %d for (%d, %d)\n", j, XYi, y);
								strip_possibility(val,&grid[y * 9 + XYi], remaining);
							}
							++y;
						}
					}
					++XYi;
				}

				found_in_region=0;
			}

			++x;
		}

// common
		++j;
	}
}

char line_of_region(const unsigned short n, unsigned short *grid, const char i)
{
	char
		x = i%9,
		y = i/9,
		Xi = 0,
		Yi = REGION_ORIG_Y(y),
		Xmax = 3,
		Ymax = Yi + 3;

	while (Xmax < 10)
	{
		while (Xi < Xmax)
		{
			while (Yi < Ymax)
			{
				//FIXME : useless ?
				if (Yi == y  && Xi == x)
				{
					++Yi;
					if (Yi == Ymax)
						break;
				}

				if(Xi == x)
				{
					if(!(grid[Yi * 9 + Xi] & n))
						return 0;
				}
				else if (grid[Yi * 9 + Xi] & n)
					return 0;

				++Yi;
			}
			Yi-=3;
			++Xi;
		}
		Xmax+=3;
	}
	//printf("\nlor : %d (%d,%d)\n", n, i/9, i%9);
	return 1;
}

char col_of_region(const unsigned short n, unsigned short *grid, const char i)
{
	char
		x = i%9,
		y = i/9,
		Xi = REGION_ORIG_X(x),
		Yi = 0,
		Xmax = Xi + 3,
		Ymax = 2;

	while (Ymax < 10)
	{
		while (Yi < Ymax)
		{
			while (Xi < Xmax)
			{
				//FIXME : useless ?
				if (Yi == y  && Xi == x)
				{
					++Xi;
					if (Xi == Xmax)
						break;
				}

				if(Yi == y)
				{
					if(!(grid[Yi * 9 + Xi] & n))
						return 0;
				}
				else if (grid[Yi * 9 + Xi] & n )
					return 0;

				++Xi;
			}
			Xi-=3;
			++Yi;
		}
		Ymax+=3;
	}
	//printf("\ncor : %d (%d,%d)\n", n, i/9, i%9);
	return 1;
}

void disjoin_chain(unsigned short *grid, char x, char y, unsigned char *remaining)
{ // waits for a 2-possibilities (a,b) cell. will try to find a (a,b,c) an a (b,c) bc cell (row,col,region)
	char
		Xi = 0,
		Yi = 0,
		Xmax = 3,
		Ymax,
		Xj,
		Yj;

	unsigned char
		first_2_poss_cell_coord = x + y * 9,
		snde_2_poss_cell_coord,
		three_poss_cell_coord,
		i;

	unsigned short
		working_cell,
		have_to_find = 0;

//line
	while(Xi < 9)
	{
		if(Xi != x)
		{
			working_cell = grid[Xi + y*9];
			if(
				number_of_possibilities(working_cell) == 2
			 && common_possibilities(grid[first_2_poss_cell_coord], working_cell) == 1
			)
			{
				snde_2_poss_cell_coord = Xi + y*9;

				have_to_find = grid[first_2_poss_cell_coord] | working_cell;
				Xj=0;
				while(Xj < 9)
				{
					working_cell = grid[Xj + y*9];
					if(working_cell == have_to_find)
					{
						three_poss_cell_coord = Xj + y*9;
						Xj = 0;
						while(Xj < 9)
						{
							i = y*9 + Xj;

							if(
								i != three_poss_cell_coord
							 && i != snde_2_poss_cell_coord
							 && i != first_2_poss_cell_coord
							)
								strip_possibilities(have_to_find, &grid[i], remaining);
							++Xj;
						}
						return;
					}
					++Xj;
				}
			}
		}
		++Xi;
	}
//cols
		while(Yi < 9)
		{
			if(Yi != y)
			{
				working_cell = grid[x + Yi*9];
				if(
					number_of_possibilities(working_cell) == 2
				 && common_possibilities(grid[first_2_poss_cell_coord], working_cell) == 1
				)
				{
					snde_2_poss_cell_coord = x + Yi*9;

					have_to_find = grid[first_2_poss_cell_coord] | working_cell;
					Yj=0;
					while(Yj < 9)
					{
						working_cell = grid[Yj*9 + x];
						if(working_cell == have_to_find)
						{
							three_poss_cell_coord = Yj*9 + x;
							Yj = 0;
							while(Yj < 9)
							{
								i = Yj*9 + x;

								if(
									i != three_poss_cell_coord
								 && i != snde_2_poss_cell_coord
								 && i != first_2_poss_cell_coord
								)
									strip_possibilities(have_to_find, &grid[i], remaining);
								++Yj;
							}
							return;
						}
						++Yj;
					}
				}
			}
			++Yi;
		}

//regions (FIXME : not tested)
		while(Xmax < 10)
		{
			Ymax=3;
			while(Ymax < 10)
			{
				while(Xi < Xmax)
				{
					Yi = Ymax - 3;
					while(Yi < Ymax)
					{
						if(Yi != y && Xi != x)
						{
							working_cell = grid[Xi + Yi*9];
							if(
								number_of_possibilities(working_cell) == 2
							 && common_possibilities(grid[first_2_poss_cell_coord], working_cell) == 1
							)
							{
								snde_2_poss_cell_coord = Xi + Yi*9;

								have_to_find = grid[first_2_poss_cell_coord] | working_cell;

								Xj=Xmax - 3;
								while(Xj < Xmax)
								{
									Yj=Ymax - 3;
									while(Yj < Ymax)
									{
										working_cell = grid[Yj*9 + Xj];
										if(working_cell == have_to_find)
										{
											three_poss_cell_coord = Yj*9 + Xj;
											Yj = 0;
											while(Yj < 9)
											{
												i = Yj*9 + Xj;

												if(
													i != three_poss_cell_coord
												 && i != snde_2_poss_cell_coord
												 && i != first_2_poss_cell_coord
												)
													strip_possibilities(have_to_find, &grid[i], remaining);
												++Yj;
											}
											return;
										}
										++Yj;
									}
									++Xj;
								}
							}
						}
						++Yi;
					}
					++Xi;
				}
				Ymax+=3;
			}
			Xmax +=3;
		}
}

void naked_subset(unsigned short *grid, unsigned char *remaining)
{ // tiggrers disjoin_chain too
	char
		x,
		y,
		Xi,
		Yi,
		Xmax=3,
		Ymax;

	unsigned short
		grid_part[9],
		working_cell;
	char
		count,
		grid_part_len=0,
		NoP;

	unsigned char
		working_coord,
		found_coords[9],
		grid_part_coords[9];

	// lines

	y=0;
	while(y < 9)
	{
		x=0;
		grid_part_len=0;

		while(x < 9)
		{
			working_cell = grid[y*9 + x];

			if(!(working_cell & RESULT_FIXED))
			{
				grid_part[grid_part_len] = working_cell;
				grid_part_coords[grid_part_len] = y*9 + x;
				++grid_part_len;

				count = is_in_grid_part(working_cell, grid_part, grid_part_len, found_coords, grid_part_coords);

				NoP=number_of_possibilities(working_cell);

				if(NoP == 2)
					disjoin_chain(grid,x,y,remaining);

				if( count == NoP)
				{
					Xi=0;
					while(Xi < 9)
					{
						working_coord = y*9 + Xi;
						if(coord_is_not_in_array(working_coord, found_coords, count))
						{
							strip_possibilities(working_cell, &grid[working_coord], remaining);
						}
						++Xi;
					}
				}
			}

			++x;
		}

		++y;
	}

	// cols
	x=0;
	while(x < 9)
	{
		y=0;
		grid_part_len=0;

		while(y < 9)
		{
			working_cell = grid[y*9 + x];

			if(!(working_cell & RESULT_FIXED))
			{
				grid_part[grid_part_len] = working_cell;
				grid_part_coords[grid_part_len] = y*9 + x;
				++grid_part_len;

				count = is_in_grid_part(working_cell, grid_part, grid_part_len, found_coords, grid_part_coords);

				if( count == number_of_possibilities(working_cell))
				{
					Yi=0;
					while(Yi < 9)
					{
						working_coord = Yi*9 + x;
						if(coord_is_not_in_array(working_coord, found_coords, count))
						{
							strip_possibilities(working_cell, &grid[working_coord], remaining); // stripping several possibilities ! (those of working cell)
						}
						++Yi;
					}
				}
			}

			++y;
		}

		++x;
	}

	// regions
//Xmax == 3, see variable definition
	while(Xmax < 10)
	{
		Ymax=3;
		while(Ymax < 10)
		{
			grid_part_len=0;
			x=Xmax-3;
			while(x < Xmax)
			{
				y=Ymax-3;
				while(y < Ymax)
				{
					working_cell = grid[y*9 + x];

					if(!(working_cell & RESULT_FIXED))
					{
						grid_part[grid_part_len] = working_cell;
						grid_part_coords[grid_part_len] = y*9 + x;
						++grid_part_len;

						count = is_in_grid_part(working_cell, grid_part, grid_part_len, found_coords, grid_part_coords);

						if( count == number_of_possibilities(working_cell))
						{
							Xi=Xmax-3;
							while(Xi < Xmax)
							{
								Yi=Ymax-3;
								while(Yi < Ymax)
								{
									working_coord = Yi*9 + Xi;
									if(coord_is_not_in_array(working_coord, found_coords, count))
									{
										strip_possibilities(working_cell, &grid[working_coord], remaining); // stripping several possibilities ! (those of working cell)
									}
									++Yi;
								}
								++Xi;
							}
						}
					}
					++y;
				}
				++x;
			}
			Ymax+=3;
		}
		Xmax+=3;
	}
}
