#ifndef BASIC_H
	#define BASIC_H

	char region(const unsigned short n, unsigned short *grid, const char i);
	char row(const unsigned short n, unsigned short *grid, const char i);
	char col(const unsigned short n, unsigned short *grid, const char i);

#endif
