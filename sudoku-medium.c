//Options : comment out options you don't want. You might not want to activate one of them.

	/* strip ability to resolve medium and difficult sudokus
	 * In order to optimize resolving easy sudokus
	 * Resolves medium sudokus too.
	 * is likely to have less bugs because it only uses basic algorithme
	 */
//#define EASY_ENGINE

// constantes :
#define _1 1
#define _2 2
#define _3 4
#define _4 8
#define _5 16
#define _6 32
#define _7 64
#define _8 128
#define _9 256
#define RESULT_FIXED 512
#define DBG  

#define QUIET 1
#define CHECK 2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARG(a) (strcmp(argv[j], "-" #a) == 0 || strcmp(argv[j], "--" #a) == 0)

char human(short n)
{
	if(n & _1)
		return 1;
	if(n & _2)
		return 2;
	if(n & _3)
		return 3;
	if(n & _4)
		return 4;
	if(n & _5)
		return 5;
	if(n & _6)
		return 6;
	if(n & _7)
		return 7;
	if(n & _8)
		return 8;
	if(n & _9)
		return 9;
	return 0;
}

char DBG_possibilities(short n)
{
	if(n & _1)
		printf("1;");
	if(n & _2)
		printf("2;");
	if(n & _3)
		printf("3;");
	if(n & _4)
		printf("4;");
	if(n & _5)
		printf("5;");
	if(n & _6)
		printf("6;");
	if(n & _7)
		printf("7;");
	if(n & _8)
		printf("8;");
	if(n & _9)
		printf("9;");
	return 0;
}

void print_grid(unsigned short *grid)
{
	unsigned char i=0;
	while(i < 81)
	{
		if(i%3 == 0)
		{
			if(i%9 == 0)
			{
				if(i)
				{
					printf("|\n");
				}
				if(i%27 == 0)
				{
					printf("-------------------------------\n");
				}
			}
			printf("|");
		}
		if(grid[i] & RESULT_FIXED)
		{
			printf(" %d ", human(grid[i]));
		}
		else
		{
			printf(" ° ");
		}

		++i;
	}
	printf("|\n");
	printf("-------------------------------\n");	
}

short value(char n)
{
	if(n == 1)
		return _1;
	if(n == 2)
		return _2;
	if(n == 3)
		return _3;
	if(n == 4)
		return _4;
	if(n == 5)
		return _5;
	if(n == 6)
		return _6;
	if(n == 7)
		return _7;
	if(n == 8)
		return _8;
	if(n == 9)
		return _9;
	return 0;
}

short result(char n)
{
	return value(n) | RESULT_FIXED;
}

char final_result(unsigned short *cell)
{
	switch(*cell)
	{
	case _1:
	case _2:
	case _3:
	case _4:
	case _5:
	case _6:
	case _7:
	case _8:
	case _9:
		*cell |= RESULT_FIXED;
		if(!(*cell & RESULT_FIXED))
		{
			printf("ARGH !\n\n");
		}
		return 1;
	}
	return 0;
}

void fill_possibilities(unsigned short *cell)
{
		*cell = _1 | _2 | _3 | _4 | _5 | _6 | _7 | _8 | _9;
}

short strip_possibility(short n, unsigned short *cell) // return val stripped of its n possibility
{
	n = value(n);
	if(*cell & n)
		*cell ^= n;

	return final_result(cell);
}

char region(char n, unsigned short *grid, char i)
{
	unsigned char
		x = i%9,
		y = i/9,
		Xi = x/3 * 3,
		Yi = y/3 * 3,
		Xmax = Xi + 3,
		Ymax = Yi + 3;

	while (Xi < Xmax)
	{
		while (Yi < Ymax)
		{
			if (Yi == y  && Xi == x)
			{
				++Yi;
				if (Yi == Ymax)
					break;
			}

			if(grid[Yi * 9 + Xi % 9] == ( value(n) | RESULT_FIXED ))
				return 1;
			++Yi;
		}
		Yi-=3;
		++Xi;
	}
	return 0;
}

char col(char n, unsigned short *grid, char i)
{// FIXME : prendre en compte : que cette colone pour ce nombre sur ce carré.
	unsigned char x = i%9, y=i/9, Yi = 0;

	while (Yi < 9)
	{
		if( Yi == y )
		{
			++Yi;
			if (Yi == 9)
				break;
		}

		if(grid[9*Yi + x] ==  (value(n) | RESULT_FIXED))
			return 1;
//		else if(!(grid[9*Yi + x] | 

		++Yi;
	}
	return 0;
}

char row(char n, unsigned short *grid, char i)
{// FIXME : prendre en compte : que cette ligne pour ce nombre sur ce carré.
	unsigned char x = i%9, y=i/9, Xi = 0;

	while (Xi < 9)
	{
		if( Xi == x )
		{
			++Xi;
			if(Xi == 9)
				break;
		}

		if(grid[9*y + Xi] == ( value(n) | RESULT_FIXED ))
			return 1;

		++Xi;
	}
	return 0;
}

#ifndef EASY_ENGINE
// Useless for easy sudokus ; find out if the given cell is the only solution of the region for a given nomber
char only_possibility(char n, unsigned short *grid, char i)
{
	unsigned char
		x = i%9,
		y = i/9,
		Xi = x/3 * 3,
		Yi = y/3 * 3,
		Xmax = Xi + 3,
		Ymax = Yi + 3;

	while (Xi < Xmax)
	{
		while (Yi < Ymax)
		{
			if (Yi == y  && Xi == x)
			{
				++Yi;
				if (Yi == Ymax)
					break;
			}

			if(grid[Yi * 9 + Xi % 9] & value(n))
			{
				return 0;
			}
			++Yi;
		}
		Yi-=3;
		++Xi;
	}
	return 1;
}
#endif

void find_posibilities (unsigned short *grid, unsigned char i, unsigned char *remaining, unsigned char o)
{
	char j=1;
	while ( j < 10)
	{
		if(grid[i] & value(j))
		{
			if(row(j, grid, i) || col(j, grid, i) || region(j, grid, i))
//				*remaining -= strip_possibility(j, &grid[i]);
			{
				if(strip_possibility(j, &grid[i]))
				{
					//fprintf(stderr, "1. Remaining : %d\n", *remaining);
					--*remaining;
					//fprintf(stderr, "1. Remaining -- : %d; set %d=>%d\n", *remaining, i, j);
					return;
				}
			}
//*/
#ifndef EASY_ENGINE

			else if (only_possibility(j, grid, i))
			{
				//fprintf(stderr, "2. Remaining : %d\n", *remaining);
				--*remaining;
				//fprintf(stderr, "2. Remaining -- : %d; set %d=>%d\n", *remaining, i, j);
				grid[i] = value(j) | RESULT_FIXED;
				return;
			}

#endif
//printf("2. rest : %d\n", *remaining);
		}
		++j;
	}
}

char grid_check(unsigned short *grid) // return 1 on error
{
	unsigned char x=0, y=0, Xstart=0, Ystart=0;
	short checking = 0, val;

// 1. lines
	while(y < 9)
	{
		x=0;
		checking = 0;
		while (x < 9)
		{
			val = value(grid[y/9 + x%9]);
			if(val & checking)
				return 1;

			checking |=val;
			++x;
		}
		++y;
	}

// 2. cols
	while(x < 9)
	{
		y=0;
		checking = 0;
		while (y < 9)
		{
			val = value(grid[y/9 + x%9]);
			if(val & checking)
				return 1;

			checking |=val;
			++y;
		}
		++x;
	}

// 3. regions
	while(Xstart < 9)
	{
		Ystart = 0;
		while(Ystart < 9)
		{
				checking = 0;
				x=Xstart; y=Ystart;

				while (x + 3 < Xstart)
				{
						while (y + 3 < Ystart)
						{
							val = value(grid[y/9 + x%9]);

							if(val & checking)
								return 1;

							checking |=val;

							++y;
						}
						++x;
				}
				Ystart+=3;
		}
		Xstart+=3;
	}

	return 0;
}

int main (int argc, char ** argv)
{
	unsigned char i = 0, j = 1, // loops
	     o = 0, // options
	     grid_done = 0,
	     remaining = 81; // remaining values to find

	unsigned short grid [81];
long dbgloop = 0;
	// arguments
	while(j < argc)
	{
		if(ARG(q) || ARG(quiet))
		{
			o |= QUIET; // We are quiet
		}
		else if (ARG(h) || ARG(help))
		{
help:			printf(
				"Sudoku solver. Raphaël JAKSE, 2010\n"
				"	sudoku-solver [options] <grid>\n"
// Example of grid :
// 000050000800900000700120300003002004680010000042000080000004002007000043005070600 difficult
// 580003000920004100000008350709020008002000700300050201046700000005900072000300046 medium
// 006700409507091060094080070250009600900057380008316002602000500700960000040008030 easy
				"	<grid> is a string composed of 0 to 9 numbers. \n"
				"	it represent a sudoku game from left to right and from top to bottom.\n"
				"	0 represents an empty value.\n\n"
				"	Available options:\n"
				"		-q,	--quiet: Behave quietly,\n"
				"		             results are given as a string like <grid>\n"
				"		-c,	--check: Check the grid when finish.\n"
				"		             if -q is passed, return EXIT_FAILURE.\n"
				"	You can't combine options, like in -qc. Write -q -c instead.\n"
			);
			return EXIT_SUCCESS;
		}
		else if (ARG(c) || ARG(check))
		{
			o |= CHECK; // we check results at the end.
		}
		else
		{
			// main argument, grid. checking, parsing argument and filling grid.
			char c; // temp number
			while(i < 81) // 9 × 9 
			{
				c = argv[j][i] - '0';
				if( c > -1 && c < 10)
				{
					if(c == 0)
					{
						fill_possibilities(&grid[i]);
					}
					else
					{
						grid[i] = value(c) | RESULT_FIXED;
						--remaining;
					}
				}
				else
				{
					c+='0';
					if(c == '\0')
					{
						//fprintf(stderr, "The grid is not complete. You must pass 81 figures.\n.");
						return EXIT_FAILURE;
					}
					else
					{
						//fprintf(stderr, "invalid argument: %c is not a valid value in the Sudoku grid.\n", c);
						return EXIT_FAILURE;
					}
				}
				++i;
			}
			i = 0;
			grid_done = 1; // We have done the grid, we can continue.
		}
		++j;
	}

	if(!grid_done) // Checking whether the grid was done.
	{ // if not, show the help.
		goto help;
	}

	// show grid
	if(!(o & QUIET))
	{
		printf("Working on this grid:\n");

		print_grid(grid);
	}


	// begin thinking
DBG //fprintf(stderr, "First remaining : %d\n", remaining);

	while(remaining)
	{
		i=0;
		while(remaining && i < 81)
		{
			if(!(grid[i] & RESULT_FIXED))
			{
				find_posibilities(grid, i, &remaining, o);
			}
			++i;
		}
		printf("Remaining : %d, loop = %ld\r", remaining, dbgloop);
DBG		++dbgloop;
DBG if(dbgloop == 1000) break;
	}

	// show final grid
	if(!(o & QUIET))
	{//FIXME : implémenter QUIET
		printf("\nResult: \n");
		print_grid(grid);
	}

	if(o & CHECK)
	{
		if(grid_check(grid))
		{
			if(!(o & QUIET))
			{
				printf("\ngrid is not correct !\n\n");
			}
			return EXIT_FAILURE;
		}
		else
		{
			if(!(o & QUIET))
			{
				printf("\ngrid seems correct.\n\n");
			}
		}
	}

	return EXIT_SUCCESS;
}
