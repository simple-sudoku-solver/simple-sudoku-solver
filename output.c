#include "values.h"
#include <stdio.h>

char human(unsigned short n)
{
	if(n & _1)
		return 1;
	if(n & _2)
		return 2;
	if(n & _3)
		return 3;
	if(n & _4)
		return 4;
	if(n & _5)
		return 5;
	if(n & _6)
		return 6;
	if(n & _7)
		return 7;
	if(n & _8)
		return 8;
	if(n & _9)
		return 9;
	return 0;
}

void print_grid(unsigned short *grid)
{
	unsigned char i=0;
	while(i < 81)
	{
		if(i%3 == 0)
		{
			if(i%9 == 0)
			{
				if(i)
				{
					printf("|\n");
				}
				if(i%27 == 0)
				{
					printf("-------------------------------\n");
				}
			}
			printf("|");
		}
		if(grid[i] & RESULT_FIXED)
			printf(" %d ", human(grid[i]));
		else
			printf(" ° ");

		++i;
	}
	printf("|\n");
	printf("-------------------------------\n");
}
