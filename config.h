/** ENGINE */

	/* Max loops number */
	    #define MAX_ITERATION 200

	/* strip ability to resolve difficult and diabolic sudokus
	 * In order to optimize resolving easy sudokus
	 * Resolves medium sudokus too.
 	 */
//	    #define EASY_ENGINE

	/* strip ability to resolve diabolic sudokus
	 * In order to optimize resolving others sudokus
 	 */
	    #define DISABLE_DIABOLIC

/** DEBUGGING */

	/* Turn on Debugging mode. NOT RECOMMENDED AT ALL. */
//	    #define DBG

	/* show values of not-found cell */
	    #define DBG_GRID

	/* show possibilities while checking for all cells */
//	    #define DBG_POSSIBILITIES

/** PROGRAM'S VALUES - PLEASE DON'T CHANGE */

	#define QUIET 1
	#define CHECK 2
