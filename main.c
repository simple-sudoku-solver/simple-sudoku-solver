#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "core.h"
#include "check.h"
#include "fill_grid.h"
#include "output.h"
#include "bruteforce.h"
#define ARG(a) (strcmp(argv[i], "-" #a) == 0 || strcmp(argv[i], "--" #a) == 0)

int main (int argc, char ** argv)
{

/** VARS */

	unsigned char
	     o = 0, // options
	     grid_done = 0,
	     remaining = 81; // remaining values to find

	int i = 1; // loops

	unsigned short grid [81];

/** ARGS */
	while(i < argc)
	{
		if(ARG(q) || ARG(quiet))
		{
			o |= QUIET; // We are quiet
		}
		else if (ARG(h) || ARG(help))
		{
help:			printf(
					"Sudoku solver. Raphaël JAKSE, 2010\n"
					"	sudoku-solver [options] <grid>\n"
					"	<grid> is a string composed of 0 to 9 numbers. \n"
					"	it represent a sudoku game from left to right and from top to bottom.\n"
					"	0 represents an empty value.\n\n"
					"	Available options:\n"
					"		-q,	--quiet: Behave quietly,\n"
					"		             results are given as a string like <grid>\n"
					"		-c,	--check: Check the grid when finish.\n"
					"		             if -q is passed, return EXIT_FAILURE.\n"
					"	You can't combine options, like in -qc. Write -q -c instead.\n"
#ifdef EASY_ENGINE
					"\nThis version was not compiled with support for difficult sudokus.\n"
#endif
			);
			return EXIT_SUCCESS;
		}
		else if (ARG(c) || ARG(check))
		{
			o |= CHECK; // we check results at the end.
		}
		else
		{
			// main argument, grid. checking, parsing argument and filling grid.
			if(fill_grid(grid, argv[i], &remaining))
				grid_done = 1; // We have done the grid, we can continue.
			else
			{
				fprintf(stderr, "Grid passed is not correct. it must length 81 chars and only contain figures.\n");
				return EXIT_FAILURE;
			}
		}
		++i;
	}

	if(!grid_done) // Checking whether the grid was done.
	{ // if not, show the help.
		goto help;
	}

/** SHOW GRID */
	if(!(o & QUIET))
	{
		printf("Working on this grid:\n");

		print_grid(grid);
	}


/** THINK */
	unsigned short loops = resolve_sudoku(grid, &remaining, o);

/** SHOW FINAL GRID */
	if(o & QUIET)
	{
		printf("%d ", remaining);
		i=0;
		while(i < 81)
		{
			printf("%c", grid[i]);
			++i;
		}
	}
	else
	{
	    if(remaining > 0)
	    {
			sudoku_brute_force(grid,&remaining, o);
			printf("%d loops, remaining %d empty cells :\n", loops, remaining);
	    }
		else
		{
			printf("Grid finished in %d loops :\n", loops);
		}
		print_grid(grid);
	}

/** CHECK GRID */
	if(o & CHECK)
	{
		if(grid_check(grid))
		{
			if(!(o & QUIET))
				printf("\ngrid is not correct !\n\n");
			return EXIT_FAILURE;
		}
		else if(!(o & QUIET))
			printf("\ngrid seems correct.\n\n");
	}

#ifdef DBG_GRID
	#include "dbg.h"

	i=0;
	while(i<81)
	{
		printf("\n(%d,%d)	", i/9 + 1, i%9 + 1);
		DBG_possibilities(grid[i]);
		++i;
	}
	puts("");
#endif

	return EXIT_SUCCESS;
}
